﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace DatabaseFileIdentification
{
    class Program
    {
        //53 51 4C 69 74 65 20 66 6F 72 6D 61 74 20 33 00
        static readonly byte[] sqliteSignature = new byte[] { 83,81,76,105,116,101,32,102,111,114,109,97,116,32,51,0};

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int memcmp(byte[] b1, byte[] b2, long count);

        static bool ByteArrayCompare(byte[] b1, byte[] b2)
        {
            // Validate buffers are the same length.
            // This also ensures that the count does not exceed the length of either buffer.  
            return b1.Length == b2.Length && memcmp(b1, b2, b1.Length) == 0;
        }

        static List<string> dbLst = new List<string>();

        static void Main(string[] args)
        {
            var allfiles = System.IO.Directory.GetFiles(@"C:\backup_name", "*.*", System.IO.SearchOption.AllDirectories);

            foreach (var fileStr in allfiles)
            {
                try
                {
                    var byteArray = File.ReadAllBytes(fileStr).Take(16).ToArray();
                    if(ByteArrayCompare(byteArray, sqliteSignature))
                        dbLst.Add(fileStr);
                }
                catch (Exception ex)
                {
                    //
                }
            }

            dbLst.ForEach(x=> Console.WriteLine(x));
        }
    }
}
